#!/bin/bash

set -o errexit -o nounset -o pipefail

echo 'Building...'
mvn package

java \
	-Djava.library.path=/curve25519-java/android/jni \
	-jar target/benchmarks.jar -f 1 -wi 8 -i 20
