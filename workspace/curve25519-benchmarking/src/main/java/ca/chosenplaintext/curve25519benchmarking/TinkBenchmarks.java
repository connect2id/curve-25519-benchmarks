package ca.chosenplaintext.curve25519benchmarking;

import java.security.*;

import com.google.crypto.tink.subtle.*;

import org.openjdk.jmh.annotations.*;

/**
 * Benchmarks of the X25519/Ed25519 implementations found in Google's Tink library.
 *
 * While trying to set this up, I realized that Tink isn't on Maven yet.
 * To work around this, the relevant source files are included directly in this
 * project instead.
 *
 * See Tink announcement on mailing list:
 * https://groups.google.com/forum/#!topic/tink-users/jOW6jIjuH18
 */
//@Warmup(iterations = 0)
//@Measurement(iterations = 0)
//@Fork(1)
public class TinkBenchmarks {
	@Benchmark
	public byte[] x25519KeyGen() {
		return Curve25519.x25519PublicFromPrivate(Curve25519.generatePrivateKey());
	}

	@State(Scope.Thread)
	public static class DHState {
		public byte[] ourPrivateKey;
		public byte[] theirPublicKey;

		@Setup(Level.Iteration)
		public void setup() {
			ourPrivateKey = Curve25519.generatePrivateKey();
			theirPublicKey = Curve25519.x25519PublicFromPrivate(Curve25519.generatePrivateKey());
		}
	}

	@Benchmark
	public byte[] x25519(DHState state) {
		return Curve25519.x25519(state.ourPrivateKey, state.theirPublicKey);
	}

	@Benchmark
	public Ed25519Sign.KeyPair ed25519KeyGen() throws GeneralSecurityException {
		return Ed25519Sign.KeyPair.newKeyPair();
	}

	@State(Scope.Thread)
	public static class Ed25519State {
		public Ed25519Sign signer;
		public Ed25519Verify verifier;
		public byte[] signature;

		@Setup(Level.Iteration)
		public void setup() throws GeneralSecurityException {
			Ed25519Sign.KeyPair keyPair = Ed25519Sign.KeyPair.newKeyPair();
			signer = new Ed25519Sign(keyPair.getPrivateKey());
			verifier = new Ed25519Verify(keyPair.getPublicKey());
			signature = signer.sign(Constants.MESSAGE);
		}
	}

	@Benchmark
	public byte[] ed25519Sign(Ed25519State state) throws GeneralSecurityException {
		return state.signer.sign(Constants.MESSAGE);
	}

	@Benchmark
	public void ed25519Verify(Ed25519State state) throws GeneralSecurityException {
		state.verifier.verify(state.signature, Constants.MESSAGE);
	}
}
