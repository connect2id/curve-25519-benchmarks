package ca.chosenplaintext.curve25519benchmarking;

import java.security.*;

import org.abstractj.kalium.*;
import org.abstractj.kalium.crypto.*;
import org.abstractj.kalium.keys.*;
import org.openjdk.jmh.annotations.*;

/**
 * Benchmarks of the public key crypto functionality in libsodium
 * (called via native bindings).
 */
//@Warmup(iterations = 0)
//@Measurement(iterations = 0)
//@Fork(1)
public class SodiumBenchmarks {
	@State(Scope.Thread)
	public static class KeyGenState {
		public SecureRandom rng;

		@Setup
		public void setup() throws NoSuchAlgorithmException, NoSuchProviderException {
			rng = SecureRandom.getInstance("NativePRNGNonBlocking", "SUN");
		}
	}

	@Benchmark
	public Point x25519KeyGen(KeyGenState state) {
		// Generate a secret key
		byte[] sk = new byte[32];
		state.rng.nextBytes(sk);
		return new Point().mult(sk);
	}

	@State(Scope.Thread)
	public static class DHState {
		public byte[] ourPrivateKey;
		public Point theirPublicKey;

		@Setup(Level.Iteration)
		public void setup(KeyGenState state) {
			ourPrivateKey = new byte[32];
			state.rng.nextBytes(ourPrivateKey);

			byte[] theirPrivateKey = new byte[32];
			state.rng.nextBytes(theirPrivateKey);
			theirPublicKey = new Point().mult(theirPrivateKey);
		}
	}

	@Benchmark
	public Point x25519(DHState state) {
		return state.theirPublicKey.mult(state.ourPrivateKey);
	}

	@Benchmark
	public SigningKey ed25519KeyGen(KeyGenState state) {
		byte[] seed = new byte[32];
		state.rng.nextBytes(seed);
		return new SigningKey(seed);
	}

	@State(Scope.Thread)
	public static class SigningState {
		public SigningKey signingKey;
		public VerifyKey verifyKey;
		public byte[] signature;

		@Setup(Level.Iteration)
		public void setup(KeyGenState state) {
			// Use default RNG, since setup is not included in benchmark
			signingKey = new SigningKey();
			verifyKey = signingKey.getVerifyKey();
			signature = signingKey.sign(Constants.MESSAGE);
		}
	}

	@Benchmark
	public byte[] ed25519Sign(SigningState state) {
		return state.signingKey.sign(Constants.MESSAGE);
	}

	@Benchmark
	public boolean ed25519Verify(SigningState state) {
		return state.verifyKey.verify(Constants.MESSAGE, state.signature);
	}
}
