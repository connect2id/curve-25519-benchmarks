*** Benchmark results data ***

See the data-XX-sorted.txt files for full benchmarking results data.

*** Running the benchmarks ***

If using Linux and Docker is installed:
-	Run `./run.sh`. This will build and run a Docker container with all needed dependencies.
-	That's it!

If not using Docker:
-	Read the Dockerfile to see what dependencies are needed.
-	workspace/curve25519-benchmarking contains the source code of a maven project.
-	See bench.sh for how to run the benchmarks.
