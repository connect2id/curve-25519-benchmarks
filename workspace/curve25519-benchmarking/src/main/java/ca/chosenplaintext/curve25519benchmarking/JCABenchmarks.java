package ca.chosenplaintext.curve25519benchmarking;

import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.*;
import java.security.spec.*;

import javax.crypto.*;

import org.openjdk.jmh.annotations.*;

/**
 * Benchmarks of the algorithms provided in the Java Cryptography Architecture.
 *
 * Explicit algorithms and crypto providers are hardcoded to ensure that
 * cross-machine comparisons are accurate.
 *
 * Algorithms:
 * - ECDH with the NIST curves P-256, P-384, P-521.
 * - ECDSA with the NIST curves P-256, P-384, P-521 (paired with SHA-256,
 *   SHA-384, SHA-512, respectively)
 * - RSA encryption with OAEP and RSA signing with PKCS#1 v1.5,
 *   using 2048-bit and 3072-bit modulus
 */
//@Warmup(iterations = 0)
//@Measurement(iterations = 0)
//@Fork(1)
public class JCABenchmarks {
	@State(Scope.Thread)
	public static class ECKeyGenState {
		public SecureRandom rng;
		public KeyPairGenerator genP256;
		public KeyPairGenerator genP384;
		public KeyPairGenerator genP521;

		@Setup
		public void setup()
		throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
			rng = SecureRandom.getInstance("NativePRNGNonBlocking", "SUN");

			genP256 = KeyPairGenerator.getInstance("EC", "SunEC");
			genP256.initialize(ECParams.EC_PARAM_P256, rng);

			genP384 = KeyPairGenerator.getInstance("EC", "SunEC");
			genP384.initialize(ECParams.EC_PARAM_P384, rng);

			genP521 = KeyPairGenerator.getInstance("EC", "SunEC");
			genP521.initialize(ECParams.EC_PARAM_P521, rng);
		}
	}

	@Benchmark
	public KeyPair ecKeyGenP256(ECKeyGenState state) {
		return state.genP256.generateKeyPair();
	}

	@Benchmark
	public KeyPair ecKeyGenP384(ECKeyGenState state) {
		return state.genP384.generateKeyPair();
	}

	@Benchmark
	public KeyPair ecKeyGenP521(ECKeyGenState state) {
		return state.genP521.generateKeyPair();
	}

	@State(Scope.Thread)
	public static class ECDHState {
		public ECPrivateKey ourPrivateKeyP256;
		public ECPublicKey theirPublicKeyP256;
		public ECPrivateKey ourPrivateKeyP384;
		public ECPublicKey theirPublicKeyP384;
		public ECPrivateKey ourPrivateKeyP521;
		public ECPublicKey theirPublicKeyP521;

		@Setup(Level.Iteration)
		public void setup(ECKeyGenState keygen) {
			ourPrivateKeyP256 = (ECPrivateKey) keygen.genP256.generateKeyPair().getPrivate();
			theirPublicKeyP256 = (ECPublicKey) keygen.genP256.generateKeyPair().getPublic();

			ourPrivateKeyP384 = (ECPrivateKey) keygen.genP384.generateKeyPair().getPrivate();
			theirPublicKeyP384 = (ECPublicKey) keygen.genP384.generateKeyPair().getPublic();

			ourPrivateKeyP521 = (ECPrivateKey) keygen.genP521.generateKeyPair().getPrivate();
			theirPublicKeyP521 = (ECPublicKey) keygen.genP521.generateKeyPair().getPublic();
		}
	}

	@Benchmark
	public byte[] ecdhP256(ECDHState state)
	throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
		KeyAgreement agree = KeyAgreement.getInstance("ECDH", "SunEC");
		agree.init(state.ourPrivateKeyP256);
		agree.doPhase(state.theirPublicKeyP256, true);
		return agree.generateSecret();
	}

	@Benchmark
	public byte[] ecdhP384(ECDHState state)
	throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
		KeyAgreement agree = KeyAgreement.getInstance("ECDH", "SunEC");
		agree.init(state.ourPrivateKeyP384);
		agree.doPhase(state.theirPublicKeyP384, true);
		return agree.generateSecret();
	}

	@Benchmark
	public byte[] ecdhP521(ECDHState state)
	throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
		KeyAgreement agree = KeyAgreement.getInstance("ECDH", "SunEC");
		agree.init(state.ourPrivateKeyP521);
		agree.doPhase(state.theirPublicKeyP521, true);
		return agree.generateSecret();
	}

	@State(Scope.Thread)
	public static class ECDSAState {
		public Signature signP256;
		public byte[] signatureP256;
		public Signature verifyP256;

		public Signature signP384;
		public byte[] signatureP384;
		public Signature verifyP384;

		public Signature signP521;
		public byte[] signatureP521;
		public Signature verifyP521;

		@Setup(Level.Iteration)
		public void setup(ECKeyGenState keygen)
		throws NoSuchAlgorithmException, NoSuchProviderException,
		SignatureException, InvalidKeyException {
			KeyPair keyPairP256 = keygen.genP256.generateKeyPair();
			signP256 = Signature.getInstance("SHA256withECDSA", "SunEC");
			signP256.initSign(keyPairP256.getPrivate());
			verifyP256 = Signature.getInstance("SHA256withECDSA", "SunEC");
			verifyP256.initVerify(keyPairP256.getPublic());

			signP256.update(Constants.MESSAGE);
			signatureP256 = signP256.sign(); // sign() resets Signature

			KeyPair keyPairP384 = keygen.genP384.generateKeyPair();
			signP384 = Signature.getInstance("SHA384withECDSA", "SunEC");
			signP384.initSign(keyPairP384.getPrivate());
			verifyP384 = Signature.getInstance("SHA384withECDSA", "SunEC");
			verifyP384.initVerify(keyPairP384.getPublic());

			signP384.update(Constants.MESSAGE);
			signatureP384 = signP384.sign(); // sign() resets Signature

			KeyPair keyPairP521 = keygen.genP521.generateKeyPair();
			signP521 = Signature.getInstance("SHA512withECDSA", "SunEC");
			signP521.initSign(keyPairP521.getPrivate());
			verifyP521 = Signature.getInstance("SHA512withECDSA", "SunEC");
			verifyP521.initVerify(keyPairP521.getPublic());

			signP521.update(Constants.MESSAGE);
			signatureP521 = signP521.sign(); // sign() resets Signature
		}
	}

	@Benchmark
	public byte[] ecdsaSignP256(ECDSAState state) throws SignatureException {
		state.signP256.update(Constants.MESSAGE);
		return state.signP256.sign();
	}

	@Benchmark
	public byte[] ecdsaSignP384(ECDSAState state) throws SignatureException {
		state.signP384.update(Constants.MESSAGE);
		return state.signP384.sign();
	}

	@Benchmark
	public byte[] ecdsaSignP521(ECDSAState state) throws SignatureException {
		state.signP521.update(Constants.MESSAGE);
		return state.signP521.sign();
	}

	@Benchmark
	public boolean ecdsaVerifyP256(ECDSAState state) throws SignatureException {
		state.verifyP256.update(Constants.MESSAGE);
		return state.verifyP256.verify(state.signatureP256);
	}

	@Benchmark
	public boolean ecdsaVerifyP384(ECDSAState state) throws SignatureException {
		state.verifyP384.update(Constants.MESSAGE);
		return state.verifyP384.verify(state.signatureP384);
	}

	@Benchmark
	public boolean ecdsaVerifyP521(ECDSAState state) throws SignatureException {
		state.verifyP521.update(Constants.MESSAGE);
		return state.verifyP521.verify(state.signatureP521);
	}

	@State(Scope.Thread)
	public static class RSAKeyGenState {
		public SecureRandom rng;
		public KeyPairGenerator gen2048;
		public KeyPairGenerator gen3072;

		@Setup
		public void setup()
		throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
			rng = SecureRandom.getInstance("NativePRNGNonBlocking", "SUN");

			// Note: public exponent "F4" is 65537

			gen2048 = KeyPairGenerator.getInstance("RSA", "SunJSSE");
			gen2048.initialize(new RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4), rng);

			gen3072 = KeyPairGenerator.getInstance("RSA", "SunJSSE");
			gen3072.initialize(new RSAKeyGenParameterSpec(3072, RSAKeyGenParameterSpec.F4), rng);
		}
	}

	@Benchmark
	public KeyPair rsaKeyGen2048(RSAKeyGenState state) {
		return state.gen2048.generateKeyPair();
	}

	@Benchmark
	public KeyPair rsaKeyGen3072(RSAKeyGenState state) {
		return state.gen3072.generateKeyPair();
	}

	@State(Scope.Thread)
	public static class RSACipherState {
		public Cipher encrypter2048;
		public Cipher decrypter2048;
		public byte[] ciphertext2048;

		public Cipher encrypter3072;
		public Cipher decrypter3072;
		public byte[] ciphertext3072;

		@Setup(Level.Iteration)
		public void setup(RSAKeyGenState keygen)
		throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException,
		NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
			KeyPair keyPair2048 = keygen.gen2048.generateKeyPair();
			encrypter2048 = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", "SunJCE");
			encrypter2048.init(Cipher.ENCRYPT_MODE, keyPair2048.getPublic(), keygen.rng);
			decrypter2048 = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", "SunJCE");
			decrypter2048.init(Cipher.DECRYPT_MODE, keyPair2048.getPrivate(), keygen.rng);
			ciphertext2048 = encrypter2048.doFinal(Constants.MESSAGE);

			KeyPair keyPair3072 = keygen.gen3072.generateKeyPair();
			encrypter3072 = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", "SunJCE");
			encrypter3072.init(Cipher.ENCRYPT_MODE, keyPair3072.getPublic(), keygen.rng);
			decrypter3072 = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", "SunJCE");
			decrypter3072.init(Cipher.DECRYPT_MODE, keyPair3072.getPrivate(), keygen.rng);
			ciphertext3072 = encrypter3072.doFinal(Constants.MESSAGE);
		}
	}

	@Benchmark
	public byte[] rsaEncrypt2048(RSACipherState state)
	throws IllegalBlockSizeException, BadPaddingException {
		return state.encrypter2048.doFinal(Constants.MESSAGE);
	}

	@Benchmark
	public byte[] rsaDecrypt2048(RSACipherState state)
	throws IllegalBlockSizeException, BadPaddingException {
		return state.decrypter2048.doFinal(state.ciphertext2048);
	}

	@Benchmark
	public byte[] rsaEncrypt3072(RSACipherState state)
	throws IllegalBlockSizeException, BadPaddingException {
		return state.encrypter3072.doFinal(Constants.MESSAGE);
	}

	@Benchmark
	public byte[] rsaDecrypt3072(RSACipherState state)
	throws IllegalBlockSizeException, BadPaddingException {
		return state.decrypter3072.doFinal(state.ciphertext3072);
	}

	@State(Scope.Thread)
	public static class RSASignState {
		public Signature signer2048;
		public Signature verifier2048;
		public byte[] signature2048;

		public Signature signer3072;
		public Signature verifier3072;
		public byte[] signature3072;

		@Setup(Level.Iteration)
		public void setup(RSAKeyGenState keygen)
		throws NoSuchAlgorithmException, NoSuchProviderException,
		SignatureException, InvalidKeyException {
			KeyPair keyPair2048 = keygen.gen2048.generateKeyPair();
			signer2048 = Signature.getInstance("SHA256withRSA", "SunRsaSign");
			signer2048.initSign(keyPair2048.getPrivate());
			verifier2048 = Signature.getInstance("SHA256withRSA", "SunRsaSign");
			verifier2048.initVerify(keyPair2048.getPublic());

			signer2048.update(Constants.MESSAGE);
			signature2048 = signer2048.sign(); // sign() resets Signature

			KeyPair keyPair3072 = keygen.gen3072.generateKeyPair();
			signer3072 = Signature.getInstance("SHA256withRSA", "SunRsaSign");
			signer3072.initSign(keyPair3072.getPrivate());
			verifier3072 = Signature.getInstance("SHA256withRSA", "SunRsaSign");
			verifier3072.initVerify(keyPair3072.getPublic());

			signer3072.update(Constants.MESSAGE);
			signature3072 = signer3072.sign(); // sign() resets Signature
		}
	}

	@Benchmark
	public byte[] rsaSign2048(RSASignState state) throws SignatureException {
		state.signer2048.update(Constants.MESSAGE);
		return state.signer2048.sign();
	}

	@Benchmark
	public byte[] rsaSign3072(RSASignState state) throws SignatureException {
		state.signer3072.update(Constants.MESSAGE);
		return state.signer3072.sign();
	}

	@Benchmark
	public boolean rsaVerify2048(RSASignState state) throws SignatureException {
		state.verifier2048.update(Constants.MESSAGE);
		return state.verifier2048.verify(state.signature2048);
	}

	@Benchmark
	public boolean rsaVerify3072(RSASignState state) throws SignatureException {
		state.verifier3072.update(Constants.MESSAGE);
		return state.verifier3072.verify(state.signature3072);
	}
}
