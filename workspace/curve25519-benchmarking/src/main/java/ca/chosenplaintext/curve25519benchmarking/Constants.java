package ca.chosenplaintext.curve25519benchmarking;

public class Constants {
	// The message that will be signed in signing benchmarks.
	// Note: longer messages will take longer to sign,
	// but that's not really worth measuring:
	// it would measure the performance of the hash function, not the
	// signature scheme.
	//
	// This constant is not final in order to prevent constant folding (which
	// would distort benchmarks).
	public static byte[] MESSAGE = {
		1, 2, 3, 4, 5, 6, 7, 8,  1, 2, 3, 4, 5, 6, 7, 8,
		1, 2, 3, 4, 5, 6, 7, 8,  1, 2, 3, 4, 5, 6, 7, 8,
		1, 2, 3, 4, 5, 6, 7, 8,  1, 2, 3, 4, 5, 6, 7, 8,
		1, 2, 3, 4, 5, 6, 7, 8,  1, 2, 3, 4, 5, 6, 7, 8,
	};
}
