FROM ubuntu:rolling
RUN apt-get update

# Install dependencies
RUN apt-get install -y curl
RUN apt-get install -y default-jdk-headless
RUN apt-get install -y gcc
RUN apt-get install -y git
RUN apt-get install -y maven
RUN apt-get install -y pkg-config

# Download Tink
RUN git clone https://github.com/google/tink /tink

# Download and build curve25519-java
RUN git clone https://github.com/WhisperSystems/curve25519-java /curve25519-java
COPY build-curve25519-jni.sh /root/
RUN /root/build-curve25519-jni.sh

# Download, build and install libsodium
RUN curl https://download.libsodium.org/libsodium/releases/libsodium-1.0.13.tar.gz \
	> /root/libsodium.tar.gz
RUN tar xfz /root/libsodium.tar.gz -C /root/
RUN mv /root/libsodium-1.0.13 /libsodium
RUN cd /libsodium \
	&& ./configure \
	&& make \
	&& make check \
	&& make install

# Run benchmarks
CMD cd /workspace/curve25519-benchmarking && ./bench.sh
