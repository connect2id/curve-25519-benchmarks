#!/bin/bash

set -o errexit -o nounset -o pipefail

cd /curve25519-java/android/jni
gcc \
	-O3 \
	-I/usr/lib/jvm/default-java/include{,/linux} \
	$(find . -type d -exec echo -I"{}" \;) \
	-shared \
	-fPIC \
	-o libcurve25519.so \
	$(find . -type f -iname '*.c')
