#!/bin/bash

set -o errexit -o nounset -o pipefail

sudo docker build -t curve25519-benchmarking .
sudo docker run -it \
	--mount type=bind,source="${PWD}/workspace",dst=/workspace \
	--mount type=bind,source="${PWD}/m2",dst=/root/.m2 \
	curve25519-benchmarking
