package ca.chosenplaintext.curve25519benchmarking;

import java.security.*;

import org.openjdk.jmh.annotations.*;
import org.whispersystems.curve25519.*;

/**
 * Benchmarks of the curve25519-java library from Open Whisper Systems (creator
 * of the Signal app).
 *
 * The Curve25519 "calculateAgreement" function in this library seems to be equivalent
 * to X25519, although I have not tested that.
 *
 * The Curve25519 "calculateSignature" function seems to perform a conversion
 * prior to signing with Ed25519 in order to allow the same key pair to be used for both signing and DH.
 * That conversion might add a small amount of overhead that would not occur
 * with plain Ed25519.
 *
 * Since the same key pairs can be used for both signing and DH, only one
 * keygen function is provided, and therefore there is only one keygen
 * benchmark.
 */
//@Warmup(iterations = 0)
//@Measurement(iterations = 0)
//@Fork(1)
public class OWSCurve25519Benchmarks {
	/**
	 * Wraps a JCA crypto RNG for use with the OWS library.
	 */
	public static class JCASecureRandomProvider implements SecureRandomProvider {
		private SecureRandom rng;

		public JCASecureRandomProvider(SecureRandom rng) {
			this.rng = rng;
		}

		public void nextBytes(byte[] output) {
			rng.nextBytes(output);
		}

		public int nextInt(int maxValue) {
			// TODO: curve25519-java implements this method incorrectly
			// I should open an issue

			throw new UnsupportedOperationException();
		}
	}

	@State(Scope.Thread)
	public static class KeyGenState {
		public Curve25519 nativeImpl;
		public Curve25519 javaImpl;

		@Setup
		public void setup() throws NoSuchAlgorithmException, java.security.NoSuchProviderException {
			// For consistency with other benchmarks, hardcode a specific RNG
			SecureRandom jcaRNG = SecureRandom.getInstance("NativePRNGNonBlocking", "SUN");
			JCASecureRandomProvider rng = new JCASecureRandomProvider(jcaRNG);

			nativeImpl = Curve25519.getInstance(Curve25519.NATIVE, rng);
			javaImpl = Curve25519.getInstance(Curve25519.JAVA, rng);
		}
	}

	@Benchmark
	public Curve25519KeyPair nativeKeyGen(KeyGenState state) {
		return state.nativeImpl.generateKeyPair();
	}

	@Benchmark
	public Curve25519KeyPair javaKeyGen(KeyGenState state) {
		return state.javaImpl.generateKeyPair();
	}

	@State(Scope.Thread)
	public static class DHState {
		public Curve25519 nativeImpl;
		public Curve25519 javaImpl;
		public byte[] ourPrivateKey;
		public byte[] theirPublicKey;

		@Setup(Level.Iteration)
		public void setup(KeyGenState keyGenState) {
			nativeImpl = keyGenState.nativeImpl;
			javaImpl = keyGenState.javaImpl;

			// Doesn't matter for setup which impl we use
			ourPrivateKey = nativeImpl.generateKeyPair().getPrivateKey();
			theirPublicKey = nativeImpl.generateKeyPair().getPublicKey();
		}
	}

	@Benchmark
	public byte[] nativeDH(DHState state) {
		return state.nativeImpl.calculateAgreement(state.theirPublicKey, state.ourPrivateKey);
	}

	@Benchmark
	public byte[] javaDH(DHState state) {
		return state.javaImpl.calculateAgreement(state.theirPublicKey, state.ourPrivateKey);
	}

	@State(Scope.Thread)
	public static class SignatureState {
		public Curve25519 nativeImpl;
		public Curve25519 javaImpl;
		public byte[] privateKey;
		public byte[] publicKey;
		public byte[] signature;

		@Setup(Level.Iteration)
		public void setup(KeyGenState keyGenState) {
			nativeImpl = keyGenState.nativeImpl;
			javaImpl = keyGenState.javaImpl;

			// Doesn't matter for setup which impl we use
			Curve25519KeyPair keyPair = nativeImpl.generateKeyPair();
			privateKey = keyPair.getPrivateKey();
			publicKey = keyPair.getPublicKey();

			signature = nativeImpl.calculateSignature(privateKey, Constants.MESSAGE);
		}
	}

	@Benchmark
	public byte[] nativeSign(SignatureState state) {
		return state.nativeImpl.calculateSignature(state.privateKey, Constants.MESSAGE);
	}

	@Benchmark
	public byte[] javaSign(SignatureState state) {
		return state.javaImpl.calculateSignature(state.privateKey, Constants.MESSAGE);
	}

	@Benchmark
	public boolean nativeVerify(SignatureState state) {
		return state.nativeImpl.verifySignature(state.publicKey, Constants.MESSAGE, state.signature);
	}

	@Benchmark
	public boolean javaVerify(SignatureState state) {
		return state.javaImpl.verifySignature(state.publicKey, Constants.MESSAGE, state.signature);
	}
}
