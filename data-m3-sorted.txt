Intel Core m3-6Y30 0.90GHz (Skylake)
Ubuntu 16.04

Benchmark                              Mode  Cnt      Score     Error  Units
SodiumBenchmarks.ed25519KeyGen        thrpt   20  13972.826 ± 165.043  ops/s
SodiumBenchmarks.x25519KeyGen         thrpt   20  12408.591 ± 171.632  ops/s
OWSCurve25519Benchmarks.javaKeyGen    thrpt   20   8772.161 ±  52.613  ops/s
TinkBenchmarks.ed25519KeyGen          thrpt   20   7371.738 ± 106.402  ops/s
OWSCurve25519Benchmarks.nativeKeyGen  thrpt   20   3855.289 ±  37.069  ops/s
TinkBenchmarks.x25519KeyGen           thrpt   20   2823.337 ±  27.354  ops/s
JCABenchmarks.ecKeyGenP256            thrpt   20    636.906 ±  11.704  ops/s
JCABenchmarks.ecKeyGenP384            thrpt   20    184.075 ±   1.909  ops/s
JCABenchmarks.ecKeyGenP521            thrpt   20    120.302 ±   1.512  ops/s
JCABenchmarks.rsaKeyGen2048           thrpt   20      2.705 ±   0.999  ops/s
JCABenchmarks.rsaKeyGen3072           thrpt   20      0.574 ±   0.262  ops/s

SodiumBenchmarks.x25519               thrpt   20  12731.948 ± 120.628  ops/s
JCABenchmarks.rsaEncrypt2048          thrpt   20   4368.543 ±  54.674  ops/s
OWSCurve25519Benchmarks.nativeDH      thrpt   20   3894.471 ±  51.418  ops/s
OWSCurve25519Benchmarks.javaDH        thrpt   20   3395.608 ±  31.589  ops/s
TinkBenchmarks.x25519                 thrpt   20   2839.271 ±  29.478  ops/s
JCABenchmarks.rsaEncrypt3072          thrpt   20   2106.148 ±  29.574  ops/s
JCABenchmarks.ecdhP256                thrpt   20    344.595 ±   3.312  ops/s
JCABenchmarks.rsaDecrypt2048          thrpt   20    129.556 ±   1.708  ops/s
JCABenchmarks.ecdhP384                thrpt   20     97.775 ±   1.635  ops/s
JCABenchmarks.ecdhP521                thrpt   20     64.428 ±   0.927  ops/s
JCABenchmarks.rsaDecrypt3072          thrpt   20     42.296 ±   0.779  ops/s

SodiumBenchmarks.ed25519Sign          thrpt   20  13542.199 ±  99.230  ops/s
TinkBenchmarks.ed25519Sign            thrpt   20   7373.040 ±  55.507  ops/s
OWSCurve25519Benchmarks.nativeSign    thrpt   20   6785.723 ±  68.546  ops/s
OWSCurve25519Benchmarks.javaSign      thrpt   20   4289.941 ±  34.556  ops/s
JCABenchmarks.ecdsaSignP256           thrpt   20    589.700 ±  18.164  ops/s
JCABenchmarks.ecdsaSignP384           thrpt   20    173.447 ±   1.695  ops/s
JCABenchmarks.rsaSign2048             thrpt   20    128.035 ±   1.305  ops/s
JCABenchmarks.ecdsaSignP521           thrpt   20    113.525 ±   0.977  ops/s
JCABenchmarks.rsaSign3072             thrpt   20     39.998 ±   0.480  ops/s

SodiumBenchmarks.ed25519Verify        thrpt   20   5246.349 ±  48.422  ops/s
OWSCurve25519Benchmarks.nativeVerify  thrpt   20   4883.035 ±  49.285  ops/s
JCABenchmarks.rsaVerify2048           thrpt   20   4286.350 ± 130.174  ops/s
OWSCurve25519Benchmarks.javaVerify    thrpt   20   2853.011 ±  52.188  ops/s
TinkBenchmarks.ed25519Verify          thrpt   20   2508.908 ±  29.941  ops/s
JCABenchmarks.rsaVerify3072           thrpt   20   2110.235 ±  24.698  ops/s
JCABenchmarks.ecdsaVerifyP256         thrpt   20    377.720 ±   5.962  ops/s
JCABenchmarks.ecdsaVerifyP384         thrpt   20    114.988 ±   1.470  ops/s
JCABenchmarks.ecdsaVerifyP521         thrpt   20     81.057 ±   0.960  ops/s
