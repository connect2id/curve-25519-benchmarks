Intel Core i7-4790 3.60GHz (Haswell)
Ubuntu 16.04

Benchmark                              Mode  Cnt      Score    Error  Units
SodiumBenchmarks.ed25519KeyGen        thrpt   20  24414.179 ± 22.816  ops/s
SodiumBenchmarks.x25519KeyGen         thrpt   20  18873.767 ± 13.914  ops/s
OWSCurve25519Benchmarks.javaKeyGen    thrpt   20  16321.367 ± 49.427  ops/s
TinkBenchmarks.ed25519KeyGen          thrpt   20  14406.537 ± 47.978  ops/s
OWSCurve25519Benchmarks.nativeKeyGen  thrpt   20   7064.201 ±  2.956  ops/s
TinkBenchmarks.x25519KeyGen           thrpt   20   5455.161 ± 24.714  ops/s
JCABenchmarks.ecKeyGenP256            thrpt   20   1176.938 ±  1.234  ops/s
JCABenchmarks.ecKeyGenP384            thrpt   20    353.863 ±  1.213  ops/s
JCABenchmarks.ecKeyGenP521            thrpt   20    229.614 ±  0.345  ops/s
JCABenchmarks.rsaKeyGen2048           thrpt   20      4.123 ±  1.181  ops/s
JCABenchmarks.rsaKeyGen3072           thrpt   20      1.066 ±  0.494  ops/s

SodiumBenchmarks.x25519               thrpt   20  19829.035 ± 13.247  ops/s
JCABenchmarks.rsaEncrypt2048          thrpt   20   7826.077 ± 50.324  ops/s
OWSCurve25519Benchmarks.nativeDH      thrpt   20   7180.837 ± 16.773  ops/s
OWSCurve25519Benchmarks.javaDH        thrpt   20   6587.457 ± 29.805  ops/s
TinkBenchmarks.x25519                 thrpt   20   5531.991 ± 24.372  ops/s
JCABenchmarks.rsaEncrypt3072          thrpt   20   3883.086 ± 20.399  ops/s
JCABenchmarks.ecdhP256                thrpt   20    626.023 ±  2.988  ops/s
JCABenchmarks.rsaDecrypt2048          thrpt   20    233.755 ±  1.551  ops/s
JCABenchmarks.ecdhP384                thrpt   20    188.424 ±  0.759  ops/s
JCABenchmarks.ecdhP521                thrpt   20    122.894 ±  0.372  ops/s
JCABenchmarks.rsaDecrypt3072          thrpt   20     79.730 ±  0.510  ops/s

SodiumBenchmarks.ed25519Sign          thrpt   20  24576.883 ± 21.443  ops/s
TinkBenchmarks.ed25519Sign            thrpt   20  14634.764 ± 57.468  ops/s
OWSCurve25519Benchmarks.nativeSign    thrpt   20  11853.164 ±  8.623  ops/s
OWSCurve25519Benchmarks.javaSign      thrpt   20   7946.984 ± 34.221  ops/s
JCABenchmarks.ecdsaSignP256           thrpt   20   1082.816 ± 10.334  ops/s
JCABenchmarks.ecdsaSignP384           thrpt   20    331.548 ±  0.571  ops/s
JCABenchmarks.rsaSign2048             thrpt   20    235.509 ±  2.163  ops/s
JCABenchmarks.ecdsaSignP521           thrpt   20    215.421 ±  0.290  ops/s
JCABenchmarks.rsaSign3072             thrpt   20     77.999 ±  0.522  ops/s

SodiumBenchmarks.ed25519Verify        thrpt   20   9643.661 ± 71.774  ops/s
OWSCurve25519Benchmarks.nativeVerify  thrpt   20   9028.707 ± 54.188  ops/s
JCABenchmarks.rsaVerify2048           thrpt   20   8598.208 ± 46.946  ops/s
OWSCurve25519Benchmarks.javaVerify    thrpt   20   5595.266 ± 43.432  ops/s
TinkBenchmarks.ed25519Verify          thrpt   20   5056.098 ± 36.686  ops/s
JCABenchmarks.rsaVerify3072           thrpt   20   3939.962 ± 15.342  ops/s
JCABenchmarks.ecdsaVerifyP256         thrpt   20    687.021 ±  3.267  ops/s
JCABenchmarks.ecdsaVerifyP384         thrpt   20    219.815 ±  1.008  ops/s
JCABenchmarks.ecdsaVerifyP521         thrpt   20    152.465 ±  0.725  ops/s
